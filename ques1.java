import java.util.Scanner;
public class ques1 {
	 
	 
	public static  int countDer (int n) {
		
		
		if(n==0||n==1)
			return 0;
		if(n==2) {
			return 1;
		}
		
		else {
			
			return ((n - 1) * (countDer(n - 1) + countDer(n - 2)));
			
		}
	}

	

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		
		int n = sc.nextInt();
		int k=countDer(n);

        System.out.println( "Count of Derangements is "
                             +k);

	}

}
