
public class ques58 {

	int value(char r) 
	  { 
	    if (r == 'I') 
	      return 1; 
	    if (r == 'V') 
	      return 5; 
	    if (r == 'X') 
	      return 10; 
	    if (r == 'L') 
	      return 50; 
	    if (r == 'C') 
	      return 100; 
	    if (r == 'D') 
	      return 500; 
	    if (r == 'M') 
	      return 1000; 
	    return -1; 
	  } 

	  int romanToInt(String s) 
	  { 
	    int total = 0; 
	    for (int i=0; i<s.length(); i++) 
	    { 
	      int str = value(s.charAt(i)); 
	      if (i+1 <s.length()) 
	      { 
	        int s2 = value(s.charAt(i+1)); 
	        if (str >= s2) 
	        { 
	          total = total + str; 
	        } 
	        else
	        { 
	          total = total - str; 
	        } 
	      } 
	      else
	      { 
	        total = total + str; 
	      } 
	    } 
	    return total; 
	  } 

	 
	  public static void main(String args[]) 
	  { 
	    ques58 ob = new ques58();
	    String val = "IV"; 
	    System.out.println(ob.romanToInt(val)); 
	  } 
	} 
