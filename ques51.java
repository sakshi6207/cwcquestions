import java.util.*;
public class ques51 {
public static int smallestpalindrome(int n) {
	int x=1;int sum;int digit;int num;
	if(n<10) {
		x=0;
		return n+1;
	}
	num=n;
	while(x!=0) {
		digit=0;sum=0;
		n=++num;
		while(n>0) {
			digit=n%10;
			sum=sum*10+digit;
			n=n/10;
		}
		if(sum==num) {
			x=0;
			return num;
		}
		else  x=1;
	}return num;
}
	public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);	
int n=sc.nextInt();
if(n>0) {
	System.out.println("next smallest palindrome:"+smallestpalindrome(n));
}
	}

}
