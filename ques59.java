import java.util.Scanner;

public class ques59 {

	 public static int[] Rotate (int arr[], int d, int n)
	  {
	    int temp;
	    for (int i = 0; i < d; i++)
	      {
		temp = arr[n - 1];
		for (int j = n - 1; j > 0; j--)
		  arr[j] = arr[j - 1];
		arr[0] = temp;
	      }
	    return arr;
	  }
	  public static void main (String[]args)
	  {
	    Scanner sc = new Scanner (System.in);
	    int d = sc.nextInt ();
	    int n = sc.nextInt ();
	    int arr[] = new int[n];
	    for (int i = 0; i < n; i++)
	      arr[i] = sc.nextInt ();

	    int r[] = Rotate (arr, d, n);

	    for (int i = 0; i < n; i++)
	      System.out.print (r[i] + " ");
	  }}